# Fusionary FED

Frontend development and build processes at Fusionary.

[![NPM](https://nodei.co/npm/fusionary-fed.png)](https://npmjs.org/package/fusionary-fed)

## Installation

Requires [Gulp 4](http://blog.reactandbethankful.com/posts/2015/05/01/how-to-install-gulp-4/).

```bash
npm install fusionary-fed --save-dev
```

…or install via [Fusionary Utils](https://bitbucket.org/fusionary/fusionary-utils):

```bash
npm install fusionary-utils -g
setup
```

## Usage

```bash
gulp <task1 task2…> <flags>
```

### CLI Flags

#### `-T`, `--tasks`
List available Gulp tasks.

#### `-d`, `--build-dev`
Sets **BUILD_ENV** environment variable to **development**.

#### `-o`, `--open`
Open a browser window when running `gulp serve`.

#### `--skip-build`
Skip the build process when deploying via Shipit.

### Environment variables

See `.env-example` for possible values.

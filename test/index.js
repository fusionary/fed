

// object variables
var childexec = require('child_process').exec;
var spawnSync = require('child_process').spawnSync;
var fs = require('fs-extra');
var path = require('path');
var chai = require('chai');
var sinonchai = require('sinon-chai');
var chaiap = require('chai-as-promised');

require('chai').should();
// var mocha = require('mocha');
// var sinon = require('sinon');
var protractor = require('protractor');
var foldersize = require('get-folder-size');

// node.js module (fallback)
var assert = require('assert');

var templatesPath = path.join(process.cwd(), 'test/fixtures/generated/test-app');

process.chdir(templatesPath);
var gulpConfig = require(path.join(templatesPath, 'gulpfile.js/config'));
var src = gulpConfig.paths.src;
var dest = gulpConfig.paths.dest;
var srcAssets = gulpConfig.paths.srcAssets;
var destAssets = gulpConfig.paths.destAssets;

chai.use(sinonchai);
chai.use(chaiap);

// Ensure we start clean
fs.ensureFileSync('.env');
fs.removeSync(path.join(destAssets));
fs.removeSync('app/assets/testextras');
fs.removeSync('app/assets/dummyfile.txt');
fs.removeSync('app/assets/css/components/testdummy.css');
fs.removeSync('app/assets/js/testdummy.js');

// somewhat hacky bull**** to get chai to work as it should (should literally is the problem)
Object.defineProperty(
  protractor.promise.Promise.prototype,
  'should',
  Object.getOwnPropertyDescriptor(Object.prototype, 'should')
);

// used for testing misc file copy behavior
var dummyfiledirs = [
  'assets',
  'assets/testextras',
  'assets/testextras/deepextras',
  'assets/testextras/deepextras/deeperextras',
  'assets/testextras/deepextras/deeperextras/deepestextras'
];

dummyfiledirs.forEach(function(dir) {
  fs.mkdirpSync(path.join('app', dir));
  fs.appendFileSync(path.join(src, dir, 'dummyfile.txt'), '467573696f6e617279');
});

// used for testing JS Linting
fs.copySync('app/assets/js/head.js', 'app/assets/js/testdummy.js');
fs.appendFileSync(path.join(srcAssets, 'js/testdummy.js'), '\stupidundefinedvariable = null;\n');

// execute gulp preptests (can't test what didn't happen)
console.log('Executing gulp \'build\' and recording...');
console.time('Gulptask');
var gulptask = spawnSync('sh', ['-c', 'gulp build build:modernizr > ../gulp.output'], {
  stdio: 'inherit'
});

console.timeEnd('Gulptask');
console.log('Completed gulp \'build!\'');

// secondary gulp task for testing css lint (errors out, thus needs containment)
var gulpcsslint = new Promise(function(resolve) {
  fs.appendFileSync(path.join(srcAssets, 'css/components/testdummy.css'),
    '/** @define MyComponent */\n\n.testdummy {}');

  childexec('gulp lint:css > ../gulp-csslint.output', function(error, stdout) {
    resolve(stdout);
  });
});

// shared read-only global variables.
// Stringify and split apart output
var gulpout = String(fs.readFileSync('../gulp.output'));

/**
 * Tests
 */

describe('Gulp:', function() {
  describe('Tasking:', function() {
    it('errorless \'gulp build\' self-report', function() {
      gulptask.status.should.equal(0);
      (typeof gulptask.error === 'undefined').should.equal(true);
    });
  });

  describe('Internals:', function() {

    // verify that every call was executed only once
    it('Calls are unique and complete', function(done) {
      var i = 0;
      var processes = [];
      var modout = gulpout.split(/('?\s'?)|(')/);
      var modtoproperty;

      while (modout.indexOf('Starting', i) !== -1) {
        i = modout.indexOf('Starting', i) + 3;
        modtoproperty = String(modout[i]);

        if (processes.hasOwnProperty(modtoproperty)) {
          processes[modtoproperty] = processes[modtoproperty] + 1;
        } else {
          processes[modtoproperty] = 1;
        }
      }
      i = 0;

      for (var prop in processes) {
        if (processes.hasOwnProperty(prop)) {
          processes[prop].should.equal(1);
          i = i + 1;
        }
      }
      console.log(`      ${i} build calls`);
      i = 0;

      while (modout.indexOf('Finished', i) !== -1) {
        i = modout.indexOf('Finished', i) + 3;
        modtoproperty = String(modout[i]);

        if (processes.hasOwnProperty(modtoproperty)) {
          processes[modtoproperty] = processes[modtoproperty] + 1;
        } else {
          // implies a process that didn't start
          console.log(`ERROR: build task ${modtoproperty} never started, yet finished.`);
          // create error mocha can recognise without stopping other tests
          assert(false);
        }
      }

      for (var prop2 in processes) {
        if (processes.hasOwnProperty(prop2)) {
          processes[prop2].should.equal(2);
        }
      }
      done();
    });
  });

  describe('Functionality:', function() {
    describe('Size Reduction:', function() {
      it('image minification', function(done) {
        var presize = fs.statSync(path.join(srcAssets, 'img/fusionary-f-blue.png')).size;
        var postsize = fs.statSync(path.join(destAssets, 'img/fusionary-f-blue.png')).size;
        var pass = presize > postsize * 2;

        console.log(`\t${presize} > ${postsize} * 2?`, `${pass}`);

        postsize.should.most(presize * 0.50);
        done();
      });

      it('Sprite collapsing', function(done) {
        foldersize('app/assets/img/sprites/sprites.svg', function(err, presize) {
          if (err) {
            throw err;
          }
          /* minimum size of default files in app.*/
          presize.should.least(85001);
          /* edgecase assurance*/
          fs.statSync(path.join(destAssets, 'img/sprites/sprites.svg')).isFile();
          fs.statSync(path.join(destAssets, 'img/sprites/sprites.svg')).size
            .should.most(presize * 0.9);
          done();
        });
      });

      it('Sprite collapsing (2)', function(done) {
        foldersize(path.join(srcAssets, 'img/sprites/sprites-other.svg'), function(err, presize) {
          if (err) {
            throw err;
          }
          /* minimum size of default files in app.*/
          presize.should.least(85001);
          /* edgecase assurance*/
          fs.statSync(path.join(destAssets, 'img/sprites/sprites-other.svg')).isFile();
          fs.statSync(path.join(destAssets, 'img/sprites/sprites-other.svg'))
            .size.should.most(presize * 0.9);
          done();
        });
      });

      it('SVG minification', function(done) {
        var presize = fs.statSync(path.join(srcAssets, 'img/svg/awesome-tiger.svg')).size;
        /* minimum size of default files in app.*/

        presize.should.least(85001);
        fs.statSync(path.join(destAssets, 'img/svg/awesome-tiger.svg')).size
          .should.most(presize * 0.9);
        done();
      });

      // it('Favicon minification', function(done) {
      //   var presize;
      //   foldersize('app/assets/img/favicons', function(err, presize) {
      //     if (err) {
      //       throw err;
      //     }
      //     /*minimum size of default files in app.*/
      //     presize.should.least(41201);
      //     foldersize(path.join(destAssets, 'img/favicons'), function(err, size) {
      //       if (err) {
      //         throw err;
      //       }
      //       size.should.most(presize * 0.9);
      //       done();
      //     });
      //   });
      // });

      it('JS minification (assumes no edits)', function() {
        var jquerysize = fs.statSync('node_modules/jquery/dist/jquery.js').size;
        var presize = fs.statSync(path.join(srcAssets, '/js/tail.js')).size;

        fs.statSync(path.join(destAssets, 'js/tail.js')).size
          .should.most(15000 + jquerysize + presize);
        /* smallest a jquery.js should be (excluding possibility of advanced sorcery).*/
        jquerysize.should.least(84000);
        /* smaller than this implies no jQuery include*/
        presize.should.least(90);
      });
    });
    describe('Compilation/Automation:', function() {
      it('Scss resolving', function(done) {
        foldersize('app/assets/css', function(err, presize) {
          if (err) {
            throw err;
          }
          /* minimum size of default files in app.*/
          presize.should.least(300);
          /* edgecase assurance*/
          fs.statSync(path.join(destAssets, 'css/app.css')).isFile();
          fs.statSync(path.join(destAssets, 'css/app.css')).size.should.least(presize * 1.1);
          done();
        });
      });

      it('CSS importing', function(done) {
        var appdata = String(fs.readFileSync(path.join(srcAssets, 'css/app.css'))).split(/\s/, 3);
        var pubdata = String(fs.readFileSync(path.join(destAssets, 'css/app.css'))).split(/\s/, 3);

        (appdata[1] !== '\'normalize.css\';' || pubdata[1] === 'normalize.css').should.equal(true);
        done();
      });

      describe('Preservation of misc files', function() {

        dummyfiledirs.forEach(function(dir) {

          it(`in ${dir}`, function(done) {
            var pubdata = String(fs.readFileSync(path.join(dest, dir, '/dummyfile.txt')));

            pubdata.should.equal('467573696f6e617279');
            done();
          });
        });
      });

      it('Webpack importing/wrapping', function(done) {
        var pubdata = String(fs.readFileSync(path.join(destAssets, 'js/head.js'))).slice(0, 41);

        // Comment is inserted via webpack; so, if it's there, webpack built the file
        pubdata.should.equal('/*! Built by Fusionary (fusionary.com) */');
        done();
      });

      it('JSLint warnings', function(done) {
        var modout = gulpout.split(/('?\n'?)|(')/);

        modout[modout.indexOf('stupidundefinedvariable') + 3]
          .should.equal(' is not defined.');
        done();
      });

      it('PostCSS warnings', function(done) {
        this.timeout(12000);
        gulpcsslint.then(function() {
          var modout = String(fs.readFileSync('../gulp-csslint.output')).split(/\n|"/);

          modout[modout.indexOf('.testdummy') - 1]
            .should.contain('Invalid component selector');
          done();
        });
      });

      it('Revision-manifest', function(done) {
        var headrev, tailrev, appcssrev;
        var pubdata = String(fs.readFileSync(path.join(destAssets, 'json/rev-manifest.json')))
          .split('"');

        headrev = pubdata[pubdata.indexOf('assets/js/head.js') + 2];
        tailrev = pubdata[pubdata.indexOf('assets/js/tail.js') + 2];
        appcssrev = pubdata[pubdata.indexOf('assets/css/app.css') + 2];

        fs.statSync(path.join(dest, headrev)).isFile();
        fs.statSync(path.join(dest, tailrev)).isFile();
        fs.statSync(path.join(dest, appcssrev)).isFile();
        done();
      });
    });
  });
});

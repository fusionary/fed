## Priority of environment variables:

1. Shell variables (e.g. `BUILD_ENV=dev gulp build`)
2. Convenience args (e.g. gulp build -d)
3. .env file
4. Defaults (`BUILD_ENV=production`)

/* jshint node:true */

module.exports = function(mixin, on) {
  var props = {};

  on = on || true;

  // All mixin args are passed as strings
  on = on && on !== 'false';

  if (on) {
    props['-webkit-font-smoothing'] = 'antialiased';
    props['-moz-osx-font-smoothing'] = 'grayscale';
  } else {
    props['-webkit-font-smoothing'] = 'subpixel-antialiased';
    props['-moz-osx-font-smoothing'] = 'auto';
  }

  return props;
};

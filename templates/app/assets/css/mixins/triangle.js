/* jshint node:true, maxparams:6 */

// Based on: https://github.com/at-import/toolkit
var parseAngle = function parseAngle(angle) {
  if (angle === 'top') {
    angle = 0;
  } else if (angle === 'top-right') {
    angle = 45 * 7;
  } else if (angle === 'right') {
    angle = 45 * 2;
  } else if (angle === 'bottom-right') {
    angle = 45 * 1;
  } else if (angle === 'bottom') {
    angle = 45 * 4;
  } else if (angle === 'bottom-left') {
    angle = 45 * 3;
  } else if (angle === 'left') {
    angle = 45 * 6;
  } else if (angle === 'top-left') {
    angle = 45 * 5;
  }

  return parseInt(angle, 10);
};

module.exports = function(mixin, color, width, height, angle) {
  color = color || 'black';
  width = width || '10px';
  height = height || '10px';
  angle = angle || 'right';
  angle = parseAngle(angle);
  var units = {
    width: width.replace(/^\d+/, ''),
    height: height.replace(/^\d+/, ''),
  };

  width = parseInt(width, 10);
  height = parseInt(height, 10);
  var props = {
    height: '0',
    width: '0',
    border: '0 solid transparent',
  };

  // offset 45deg to make each side start at 0
  var deg = angle + 45;

  // shift to be on a scale from 0 to 90.
  while (deg > 90) {
    deg = deg - 90;
  }

  while (deg < 0) {
    deg = deg + 90;
  }

  // Get a ratio of 90 to multiply by.
  deg = deg / 90;

  if (angle <= 45 || angle > 315) {
    props['border-bottom-color'] = color;
    props['border-width'] = [
      0,
      (width * Math.abs(deg - 1)) + units.width,
      height + units.height,
      (width * deg) + units.width,
    ].join(' ');
  }

  if (angle > 45 && angle <= 135) {
    props['border-left-color'] = color;
    props['border-width'] = [
      (height * deg) + units.height,
      0,
      (height * Math.abs(deg - 1)) + units.height,
      width + units.width,
    ].join(' ');
  }

  if (angle > 135 && angle <= 225) {
    props['border-top-color'] = color;
    props['border-width'] = [
      height + units.height,
      (width * deg) + units.width,
      0,
      (width * Math.abs(deg - 1)) + units.width,
    ].join(' ');
  }

  if (angle > 225 && angle <= 315) {
    props['border-right-color'] = color;
    props['border-width'] = [
      (height * Math.abs(deg - 1)) + units.height,
      width + units.width,
      (height * deg) + units.height,
      0,
    ].join(' ');
  }

  return props;
};

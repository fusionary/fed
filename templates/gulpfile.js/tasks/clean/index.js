var path   = require('path');
var config = require(path.join(process.cwd(), 'gulpfile.js/config'));
var gulp   = require('gulp');
var del    = require('del');

gulp.task('clean', function() {
  return del([path.join(config.paths.destAssets, '**/*')]);
});

gulp.task('clean:js', function() {
  return del([path.join(config.paths.destAssets, 'js/**/*.js')]);
});

gulp.task('clean:css', function() {
  return del([path.join(config.paths.destAssets, 'css/**/*.css')]);
});

gulp.task('clean:rev-manifest', function() {
  return del([path.join(config.paths.destAssets, 'json/rev-manifest.json')]);
});

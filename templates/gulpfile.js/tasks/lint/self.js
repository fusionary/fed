var path         = require('path');
var config       = require(path.join(process.cwd(), 'gulpfile.js/config'));
var gulp         = require('gulp');
var eslint      = require('gulp-eslint');
var chalk       = require('chalk');

var displayResults = function displayResults(results) {
  if (!results.warningCount && !results.errorCount) {
    var msg = `All ${results.length} files Linty Fresh!™`;

    return console.log(chalk.green(msg));
  }
};

var src = [
  path.join(config.paths.root, 'gulpfile.js/**/*.js'),
];

gulp.task('lint:self', function() {
  return gulp.src(src)
  .pipe(eslint())
  .pipe(eslint.formatEach())
  .pipe(eslint.results(displayResults));
});

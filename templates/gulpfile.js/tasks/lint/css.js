/**
 * Not running this as part of build:css because of its
 * propencity to break, since we're linting SUIT files only.
 */

var path    = require('path');
var config  = require(path.join(process.cwd(), 'gulpfile.js/config'));
var gulp    = require('gulp');
var postcss = require('gulp-postcss');

var src = [
  path.join(config.paths.srcAssets, 'css/**/*.css'),
  `!${path.join(config.paths.srcAssets, 'css/vendor/**/*.css')}`,
];

gulp.task('lint:css', function() {
  return gulp.src(src)
  .pipe(postcss(config.postcss.lintPlugins));
});

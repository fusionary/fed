require('require-dir')();
var gulp = require('gulp');

gulp.task('lint', gulp.parallel(
  'lint:js',
  'lint:css'
));

var path          = require('path');
var config        = require(path.join(process.cwd(), 'gulpfile.js/config'));
var gulp          = require('gulp');
var shipitCaptain = require('shipit-captain');

gulp.task('shipit', function(cb) {
  shipitCaptain(config.shipit.config, {
    init: config.shipit.init
  }, cb);
});

gulp.task('deploy', function(cb) {
  shipitCaptain(config.shipit.config, {
    init: config.shipit.init,
    run: 'deploy'
  }, cb);
});

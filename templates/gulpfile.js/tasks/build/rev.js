var path         = require('path');
var config       = require(path.join(process.cwd(), 'gulpfile.js/config'));
var gulp         = require('gulp');
var rev          = require('gulp-rev');
var jsonDest     = path.join(config.paths.destAssets, 'json');
var browserSync  = require('browser-sync').get('serve');
var gulpif       = require('gulp-if');

var src = [
  path.resolve(config.paths.destAssets, '**/*.css'),
  path.resolve(config.paths.destAssets, '**/*.js')
];

gulp.task('build:rev', function() {
  return gulp.src(src, {base: config.paths.dest})
  .pipe(rev())
  .pipe(gulp.dest(config.paths.dest))
  .pipe(rev.manifest())
  .pipe(gulpif(!browserSync.active, gulp.dest(jsonDest)));
});

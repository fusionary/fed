require('require-dir')('./', {recurse: true});
require('../lint');
require('../clean');
var gulp   = require('gulp');
var path   = require('path');
var config = require(path.join(process.cwd(), 'gulpfile.js/config'));

var taskSeries = [
  'clean',
  gulp.parallel([
    'build:js',
    'build:css',
    'build:img',
    'build:misc'
  ]),
  'build:rev',
  'build:html',
  'lint:self',
].filter((item) => {
  return !(item === 'build:html' && !config.staticTemplates);
});

gulp.task('build',
  gulp.series(taskSeries)
);

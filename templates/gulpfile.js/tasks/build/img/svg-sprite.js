/**
 * @see https://github.com/gulpjs/gulp/blob/master/docs/recipes/running-task-steps-per-folder.md
 */

var fs          = require('fs');
var path        = require('path');
var config      = require(path.join(process.cwd(), 'gulpfile.js/config'));
var gulp        = require('gulp');
var svgSprite   = require('gulp-svg-sprite');
var src         = path.join(config.paths.srcAssets, 'img/svg/**/*.svg');
var dest        = path.join(config.paths.destAssets, 'img/sprites/');

gulp.task('build:img:svg-sprite', function() {
  return gulp.src(src)
  .pipe(svgSprite({
    mode: {
      inline: true,
      symbol: {
        dest: '.',
        sprite: 'sprites',
      },
    }
  }))
  .pipe(gulp.dest(dest));
});

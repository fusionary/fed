require('require-dir')();
var gulp = require('gulp');

gulp.task('build:img', gulp.parallel(
  'build:img:optimize',
  'build:img:svg-sprite'
));

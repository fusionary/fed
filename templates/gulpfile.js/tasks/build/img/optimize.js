var path     = require('path');
var config   = require(path.join(process.cwd(), 'gulpfile.js/config'));
var gulp     = require('gulp');
var gulpif = require('gulp-if');
var imagemin = require('gulp-imagemin');
var imageminPngquant = require('imagemin-pngquant');
var mozJpeg = require('imagemin-mozjpeg');

var dest     = path.join(config.paths.destAssets, '/img');

var exceptions = {
  compressed: path.join(config.paths.srcAssets, 'img/**/*.compressed.jpg'),
  alpha: path.join(config.paths.srcAssets, 'img/**/*.alpha.png'),
  sprites: path.join(config.paths.srcAssets, '/img/sprites/**/*'),
};

var src = [
  path.join(config.paths.srcAssets, '/img/**/*'),
];

// Exclude exceptions for the main build:img:optimize task
Object.keys(exceptions).forEach(function(exception) {
  src.push(`!${exceptions[exception]}`);
});

gulp.task('build:img:optimize:other', function() {
  return gulp.src(src)
  .pipe(gulpif(
    process.env.BUILD_ENV !== 'development',
    imagemin({
      progressive: true,
      optimizationLevel: 3,
      svgoPlugins: []
    })
  ))
  .pipe(gulp.dest(dest));
});

gulp.task('build:img:optimize:alpha', function() {
  return gulp.src(exceptions.alpha)
  .pipe(gulpif(
    process.env.BUILD_ENV !== 'development',
    imageminPngquant()()
  ))
  .pipe(gulp.dest(dest));
});

gulp.task('build:img:optimize:compress', function() {
  return gulp.src(exceptions.compressed)
  .pipe(gulpif(
    process.env.BUILD_ENV !== 'development',
    mozJpeg({quality: 30})()
  ))
  .pipe(gulp.dest(dest));
});

gulp.task('build:img:optimize', gulp.parallel(
  'build:img:optimize:other',
  'build:img:optimize:alpha',
  'build:img:optimize:compress'
));

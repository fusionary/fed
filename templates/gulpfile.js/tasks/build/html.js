var path           = require('path');
var config         = require(path.join(process.cwd(), 'gulpfile.js/config'));
var gulp           = require('gulp');
var views          = path.join(config.paths.src, 'views');
var dest           = config.paths.dest;
var nunjucksRender = require('gulp-nunjucks-render');
var data           = require('gulp-data');

var src = [
  path.join(views, '**/*.{nj,nunjucks}'),
  `!${path.join(views, '_*/*')}`,
];

gulp.task('build:html', function() {
  var revManifest;

  nunjucksRender.nunjucks.configure(
    [views],
    {watch: false}
  );

  try {
    revManifest = require(path.join(config.paths.destAssets, 'json/rev-manifest'));
  } catch (err) {
    revManifest = revManifest ? {revManifest: revManifest} : {};
  }

  return gulp.src(src)
  .pipe(data(require(path.join(views, '_data/default.json'))))
  .pipe(data(revManifest))
  .pipe(nunjucksRender())
  .pipe(gulp.dest(dest));
});

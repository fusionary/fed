var path      = require('path');
var config    = require(path.join(process.cwd(), 'gulpfile.js/config'));
var gulp      = require('gulp');
var modernizr = require('gulp-modernizr');
var dest      = path.join(config.paths.srcAssets, 'js/vendor/modernizr.js');

var src = [
  path.join(config.paths.srcAssets, '{js,css}/**/*'),
  `!${dest}`,
];

// Not using gulp.dest because it creates a bunch of empty folders
gulp.task('build:modernizr', function() {
  return gulp.src(src)
  .pipe(modernizr({
    dest: dest,
    options: [
      'setClasses',
      'addTest',
      'fnBind',
      'testProp'
    ]
  }));
});

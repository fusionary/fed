var path   = require('path');
var config = require(path.join(process.cwd(), 'gulpfile.js/config'));
var gulp   = require('gulp');
var dest   = config.paths.destAssets;

var src = [
  path.join(config.paths.srcAssets, '**/*'),
  `!${path.join(config.paths.srcAssets, '{css,img,js}/**/*')}`,
];

gulp.task('build:misc', function() {
  return gulp.src(src)
  .pipe(gulp.dest(dest));
});

var path        = require('path');
var config      = require(path.join(process.cwd(), 'gulpfile.js/config'));
var gulp        = require('gulp');
var favicons    = require('favicons');
var faviconPath = '/assets/img/favicons/';

gulp.task('build:favicons', function(done) {
  favicons({
    files: {
      src: path.join(config.paths.src, faviconPath, '/default.png'),
      dest: path.join(config.paths.src, faviconPath),
      html: path.join(config.paths.src, 'views/_includes/favicons.html'),
      iconsPath: faviconPath,
    },
    icons: {
      appleIcon: true,
      appleStartup: true,
      favicons: true,
      android: false,
      coast: false,
      firefox: false,
      opengraph: false,
      windows: false,
      yandex: false
    },
  }, done);
});

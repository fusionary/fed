var path        = require('path');
var config      = require(path.join(process.cwd(), 'gulpfile.js/config'));
var gulp        = require('gulp');
var browserSync = require('browser-sync').get('serve');

gulp.task('serve:watch', function() {

  var reloadBrowser = function reloadBrowser(done) {
    browserSync.reload();
    done();
  };

  /**
   * Watching js src because webpack-dev-middleware is smart
   * and delays requests until the compiling has finished:
   * https://github.com/webpack/webpack-dev-middleware
   *
   * CSS is reloaded in build:css with browserSync.stream
   */
  gulp.watch([
    path.join(config.paths.srcAssets, '**/*.js{,x}'),
    path.join(config.paths.destAssets, '/{fonts,img,json}/**/*.*'),
    path.join(config.paths.dest, '**/*.html'),
  ], reloadBrowser);

  if (config.staticTemplates) {
    // Build static templates into the dest directory (which then triggers a reload from watch above)
    gulp.watch([
      path.join(config.paths.src, 'views/**/*.{html,njk,nunjucks}'),
    ], gulp.series(
      'build:html'
    ));
  } else {
    // If no static templates (using CMS or some such), reload browser when src templates change
    gulp.watch([
      path.join(config.paths.src, 'views/**/*.*'),
    ], reloadBrowser);
  }

  gulp.watch([
    path.join(config.paths.srcAssets, '/css/**/*.css'),
  ], gulp.series(
    'build:css'
  ));

  gulp.watch([
    path.join(config.paths.srcAssets, '/img/**/*'),
    `!${path.join(config.paths.srcAssets, '/img/sprites/**/*')}`
  ], gulp.series(
    'build:img:optimize'
  ));

  gulp.watch([
    path.join(config.paths.srcAssets, '/img/sprites/*.svg/*.svg')
  ], gulp.series(
    'build:img:svg-sprite'
  ));
});

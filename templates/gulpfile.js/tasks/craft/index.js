var gulp = require('gulp');
var exec = require('child_process').exec;
var chalk = require('chalk');

var execTask = function(command) {
  return function(cb) {
    console.log(chalk.gray('Running command: ') + chalk.green(command));
    exec(command, cb);
  };
};

gulp.task('craft:permissions', gulp.series([
  execTask('chmod -R a+rw craft/app'),
  execTask('chmod -R a+rw craft/config'),
  execTask('chmod -R a+rw craft/storage'),
  execTask('chmod -R a+rw public/storage'),
]));

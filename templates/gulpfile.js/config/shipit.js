var app = '';
var user = '';
var server = '';
var util = require('util');
var path = require('path');
var deployTo = util.format('/home/%s/apps/%s/', user, app);
var argv = require('yargs')
.option('skip-build', {
  type: 'boolean',
  default: false
}).argv;

module.exports = function(config) {
  var shipitConfig = {
    default: {
      workspace: util.format('/tmp/%s', config.pkg.name),
      repositoryUrl: config.pkg.repository.url,
      keepReleases: 3,
      shallowClone: false,
      servers: util.format('%s@%s', user, server),
      ignores: [
        '.git',
        'node_modules',
      ],
      shared: {
        overwrite: true,
        files: [
          '.env'
        ],
        dirs: [
          'public/storage',
          'db',
        ],
      },
      slack: {
        webhookUrl: 'https://hooks.slack.com/services/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',
        message: {
          channel: '#deployments'
        }
      },
      assets: {
        paths: ['public/storage'],
        excludePatterns: [],
      },
      db: {
        local: {
          host: process.env.DB_HOST,
          socket: process.env.DB_SOCKET,
          database: process.env.DB_NAME,
          username: process.env.DB_USER,
          password: process.env.DB_PASSWORD,
        },
        remote: {
          host: '127.0.0.1',
        },
      }
    },
    development: {
      deployTo: path.join(deployTo, 'development'),
      db: {
        remote: {
          database: '',
          username: '',
          password: '',
        }
      }
    },
  };

  if (argv.skipBuild) {
    shipitConfig.default.ignores.push('public/assets');
  }

  var shipitInit = function(shipit) {
    require('shipit-fusionary')(shipit);
    require('shipit-deploy')(shipit);
    require('shipit-shared')(shipit);
    require('shipit-ssh')(shipit);
    require('shipit-assets')(shipit);
    require('shipit-db')(shipit);
    shipit.initConfig(shipitConfig);

    shipit.blTask('build', function() {
      return Promise.resolve()
      .then(function() {
        if (argv.skipBuild) {
          return Promise.resolve();
        }

        return shipit.local('NODE_ENV=development npm install', {
          cwd: shipit.config.workspace
        })
        .then(function() {
          return shipit.local('BUILD_ENV=production gulp build', {
            cwd: shipit.config.workspace
          });
        });
      })
      .then(function() {
        return shipit.local('if ( [ -e "composer.json" ] ); then composer install; fi', {
          cwd: shipit.config.workspace
        });
      });
    });

    shipit.on('fetched', function() {
      shipit.start('build');
    });

    shipit.blTask('setRemoteEnv', function() {
      var file = path.join(shipit.config.deployTo, 'current/.env');

      return shipit.remote(`if [ -f ${file} ]; then cat ${file} 2>/dev/null; fi;`).then(function(response) {
        var file = response[0].stdout.trim();
        var remoteEnv = require('dotenv').parse(file);

        shipit.config.db.remote = {
          host: remoteEnv.DB_HOST,
          database: remoteEnv.DB_NAME,
          username: remoteEnv.DB_USER,
          password: remoteEnv.DB_PASSWORD,
        };

        return shipit.dotenv;
      });
    });

    shipit.on('db', function() {
      return shipit.start('setRemoteEnv');
    });

  };

  return {
    config: shipitConfig,
    init: shipitInit,
  };
};

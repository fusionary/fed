var slugify = require('slugify');

'use strict';
module.exports = function(env) {
  var replacer = function replacer(key, val) {
    if (typeof val === 'string') {

      val = val

      // left single quote
      .replace(/(^|[\-\u2014\s(\["])'/g, '$1\u2018')

      // right single quote
      .replace(/'/g, '\u2019')

      .replace(/--/g, '\u2014')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;');
    }

    return val;
  };

  var toSlug = function toSlug(data) {
    data = data || '';

    return slugify(data).toLowerCase();
  };

  /**
   * Filters
   */

  env.addFilter('kebab', toSlug);
  env.addFilter('slugify', toSlug);

  env.addFilter('json_encode', function(data) {
    if (typeof (data) === 'object') {
      return JSON.stringify(data);
    }

    data += '';

    return data;
  });

  env.addFilter('json_encode_attr', function(data) {
    if (typeof (data) === 'object') {
      return JSON.stringify(data, replacer);
    }

    data += '';

    return data;
  });

  // *** nl2br ***
  // If you need to use the `striptags` filter for the same data,
  // use that instead, with the `preserve_linebreaks` argument set to `true`
  // e.g. {{ mydata | striptags(true) }}
  // If sharing the template with Twig, still use the `preserve_linebreaks` arg,
  // but also include `nl2br` for Twig
  // e.g. {{ myData | striptags(true) | nl2br }}
  env.addFilter('nl2br', function(data) {
    // Based on the Nunjucks `striptags` filter's `preserve_linebreaks` argument
    // https://github.com/mozilla/nunjucks/blob/master/src/filters.js#L445-L450

    if (data == null) {
      data = '';
    }
    var output = data
    // Trim entire string
    .replace(/^\s+|\s+$/g, '')
    // remove leading and trailing spaces from each line
    .replace(/^ +| +$/gm, '')
    // squash adjacent spaces
    .replace(/ +/g, ' ')
    // normalize linebreaks (CRLF -> LF)
    .replace(/(\r\n)/g, '\n')
    // squash abnormal adjacent linebreaks
    .replace(/\n\n\n+/g, '\n\n');

    return output;
  });

  // indexOf filter
  // e.g. {{ haystack | indexOf('needle') }}
  env.addFilter('indexOf', function(haystack, needle) {
    if (!haystack || !needle) {
      return -1;
    }

    return haystack.indexOf(needle);
  });

  // contains filter
  // e.g. {{ haystack | contains('needle') }}
  env.addFilter('contains', function(haystack, needle) {
    if (!haystack || !needle) {
      return false;
    }

    return haystack.indexOf(needle) !== -1;
  });


  env.addFilter('merge', function(tgt, src) {
    var merged;

    if (Array.isArray(tgt)) {

      src = src || [];
      merged = [].concat(tgt);

      src.forEach(function(item) {
        if (merged.indexOf(item) === -1) {
          merged.push(item);
        }
      });

    } else if (typeof tgt === 'object' && typeof src === 'object') {
      merged = Object.assign({}, tgt, src);
    } else {
      merged = tgt;
    }

    return merged;
  });
};

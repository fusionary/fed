var path = require('path');
var webpack = require('webpack');
var yargs = require('yargs');
var argv = yargs.option('verbose', {
  alias: 'v',
  default: false
}).argv;

module.exports = function(config) {
  var rules = [
    {
      test: /\.jsx?$/,
      include: [
        /app\/assets\/js/,
        /fmjs/,
      ],
      loader: 'babel-loader',
    },
    {
      test: /\.(njk|nj|nunjucks|twig)$/,
      loader: 'nunjucks-loader',
      options: {
        config: `${__dirname}/nunjucks.js`
      },
    },
  ];

  var exposeLoaders = [
    {
      test: 'jquery',
      use: [
        'expose-loader?$',
        'expose-loader?jQuery'
      ],
    },
    {
      test: 'react',
      loader: 'expose-loader?React',
    },
    {
      test: 'webfontloader',
      loader: 'expose-loader?WebFont',
    },
  ];

  exposeLoaders.forEach(function(item) {
    try {
      var test = require.resolve(item.test);

      item.test = test;
      rules.push(item);
    } catch (e) {
      if (argv.verbose) {
        console.log(`Skipping "${item.loader}". Webpack loader not found.`);
      }
    }
  });

  var webpackConfig = {
    entry: {
      head: [path.join(config.paths.srcAssets, 'js/head.js')],
      tail: [
        // 'babel-polyfill',
        path.join(config.paths.srcAssets, 'js/tail.js')
      ],
    },
    output: {
      filename: '[name].js',
      chunkFilename: '[name].js',
      path: path.join(config.paths.destAssets, 'js'),
    },
    resolve: {
      modules: [
        'node_modules',
        path.join(config.paths.srcAssets, 'js'),
        path.join(config.paths.src, 'views'),
      ],
    },
    module: {
      rules: rules,
    },
    devServer: {
      host: 'localhost',
      port: 8080,
    },
    plugins: [
      new webpack.BannerPlugin({
        banner: 'Built by Fusionary (fusionary.com)',
        entryOnly: true
      })
    ]
  };

  let envPlugins = [];

  if (process.env.BUILD_ENV === 'development') {
    webpackConfig.devtool = 'cheap-module-source-map';
    envPlugins = [
      new webpack.LoaderOptionsPlugin({
        debug: true
      })
    ];
  } else {
    let uglifyOptions = {
      compress: {
        warnings: false,
        drop_console: true // eslint-disable-line camelcase
      },
    };

    envPlugins = [
      // Apparently some libs (React) may use process.env.NODE_ENV, so we better set it.
      // https://github.com/facebook/react/blob/master/docs/docs/getting-started.md#using-react-from-npm
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify('production')
        }
      }),
      new webpack.optimize.UglifyJsPlugin(uglifyOptions)
    ];
  }

  webpackConfig.plugins.push(...envPlugins);

  return webpackConfig;
};

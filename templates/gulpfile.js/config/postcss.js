var path = require('path');

/**
 * Reminder: Order is important
 */

module.exports = function(config) {
  var srcDir   = path.join(config.paths.srcAssets, 'css');
  var plugins  = [
    require('postcss-mixins')({
      mixinsDir: path.join(srcDir, 'mixins'),
    }),
    require('postcss-easy-import')(),
    require('postcss-assets')({
      basePath: config.paths.srcAssets,
      baseUrl: '/assets/',
    }),
    require('postcss-cssnext')(),
    require('postcss-easings')(),
    require('postcss-extend')(),
    require('postcss-pseudo-class-enter')(),
    require('postcss-property-lookup')(),
    require('postcss-advanced-variables')(),
    require('postcss-reporter')(),
  ];

  var prodPlugins = [
    require('cssnano')({
      autoprefixer: false,
      discardUnused: false,
      zindex: false,
      reduceIndents: false,
      mergeIndents: false,
    }),
  ];

  var lintPlugins = [
    require('stylelint')(),
    require('postcss-reporter')(),
  ];

  return {
    plugins: plugins,
    lintPlugins: lintPlugins,
    prodPlugins: prodPlugins,
  };
};

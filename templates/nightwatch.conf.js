'use strict';
/* eslint-disable camelcase */
const path = require('path');
const fs = require('fs');
const os = require('os');

require('dotenv').config({silent: true});

const isFed = /\/templates$/.test(__dirname);
let testBin = process.env.TEST_BIN;

if (isFed && !testBin) {
  testBin = path.join(os.homedir(), 'bin');
}

const seleniumConfig = {
  start_process: true,
  host: '127.0.0.1',
  port: 4444,
  server_path: path.join(testBin, 'selenium-server-standalone.jar'),
  cli_args: {
    'webdriver.chrome.driver': path.join(testBin, 'chromedriver'),
  }
};

let ffPath = path.join(testBin, 'Firefox');

try {
  let stat = fs.statSync(`${ffPath}.app`);

  if (stat.isFile() || stat.isDirectory()) {
    seleniumConfig.cli_args['webdriver.firefox.bin'] = ffPath;
  }
} catch (e) {
  throw (new Error(`You need an older Firefox app installed at ${ffPath}`));
}

const testSettings = {
  default: {
    launch_url: 'http://localhost',
    selenium_port: 4444,
    selenium_host: 'localhost',
    skip_testcases_on_fail: false,
    exclude: [
      'init.js'
    ],
    desiredCapabilities: {
      browserName: 'chrome',
      javascriptEnabled: true,
      acceptSslCerts: true
    }
  },
  firefox: {
    desiredCapabilities: {
      browserName: 'firefox',
      // Undeclared settings Inherit from 'default'
    }
  }
};

module.exports = {
  src_folders: ['test/nightwatch'],
  selenium: seleniumConfig,
  test_settings: testSettings
};

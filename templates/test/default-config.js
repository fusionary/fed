// These config options apply to all pages listed in pages.js
// unless a page overrides them

let errors = {
  severe: true,
  resource: true,
  deprecation: true,
  warning: true,
  css: false,
};

let config = {
  errors: errors,
  verbose: false,
  response: 200,
  globalExposedVars: [],
  globalProtectedVars: [],
  responseHeaders: [],
  containsText: [],
  containsElement: [],
  containsVisibleElement: [],
  containsHiddenElement: [],
  logWhitelist: [
    /google|pubads/,
    /chrome\-extension/,
  ],
};

module.exports = config;

const path = require('path');
const url = require('url');
const defaultConfig = require('../default-config');

let testEnv = process.env.TEST_ENV;

module.exports = {
  filterMapRequests: (requests, domain) => {
    return requests
    .filter((item) => {
      if (!item.url && !item.path) {
        return false;
      }

      let envs = item.environments;

      if (envs && envs.length && !envs.includes(testEnv)) {
        return false;
      }

      return true;
    })
    .map((item) => {
      if (!item.url && item.path) {
        item.url = url.resolve(domain, item.path);
      }

      return item;
    });
  },

  mergeConfig: (pageConfig) => {
    let config = Object.assign({}, defaultConfig, pageConfig);

    let tmpErrors = Object.assign({}, defaultConfig.errors, pageConfig.errors);

    for (let key in tmpErrors) {
      config.errors[key] = {
        count: 0,
        test: tmpErrors[key]
      };
    }

    return config;
  },

  whitelist: (list, msg) => {
    return list.reduce((previous, current) => {
      return previous || current.test(msg);
    }, false);
  }
};

module.exports = {
  'extends': 'kswedberg',
  globals: {
    describe: true,
    it: true
  },
  'indent': ['error', 2],
};

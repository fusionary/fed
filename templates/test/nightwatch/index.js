'use strict';
const path = require('path');
const url = require('url');
const chalk = require('chalk');
const request = require('request');
const os = require('os');

let defaultConfig = require(path.join('..', 'pages'));
let tests = require(path.join('..', 'pages'));
let utils = require('../utils');

let testEnv = process.env.TEST_ENV;
let domain = tests.domains[testEnv];
let isFed = /templates\/test\/nightwatch$/.test(__dirname);
let testPages = utils.filterMapRequests(tests.requests, domain);
var nwTests = {};

// Only do this in fusionary fed
if (isFed && !testPages.length) {
  testEnv = testEnv || 'production';
  domain = tests.domains[testEnv];
  tests = require(path.join('..', 'pages-example'));
  testPages = utils.filterMapRequests(tests.requests, domain);
} else if (!testEnv || !domain) {
  let msg = 'You need to add TEST_ENV to your .env file. One of: ';

  msg += Object.keys(tests.domains).join(', ');
  throw new Error(msg);
}

testPages.forEach((cConfig, i) => {
  let idx = i + 1;

  // Programmatically create a test function for every page
  nwTests[`Test ${idx} ${cConfig.url}`] = function(client) {
    let pageConfig = utils.mergeConfig(cConfig);
    let errors = pageConfig.errors;

    client
    // Make early raw request for headers and response code
    .perform(function() {
      let clientSelf = this;
      let uAgent = client.capabilities.browserName;
      let vNum = client.capabilities.version;

      uAgent = uAgent = `${uAgent[0].toUpperCase + uAgent.slice(1).toLowerCase()}/${vNum}`;

      let rOptions = {
        url: pageConfig.url,
        method: 'GET'
      };

      request(rOptions, function(err, response, body) {
        if (err) {
          return;
        }
        client.assert.strictEqual(response.statusCode, pageConfig.response, `Page gave correct response code of ${pageConfig.response}`);

        if (pageConfig.responseHeaders && pageConfig.responseHeaders.length) {
          pageConfig.responseHeaders.forEach(function(searcher) {
            if (searcher.name) {
              if (searcher.pattern) {
                client.assert.ok(searcher.pattern.test(response.headers[searcher.name]), `Header ${searcher.name} defined and contains matching pattern`);
              } else {
                client.assert.ok(!!response.headers[searcher.name], `Header ${searcher.name} defined`);
              }
            }
          });
        }
      });
    })

    // Navigate
    .url(pageConfig.url)

    // Verify document title & url redirection (if any "to" specified)
    .perform(function() {
      if (pageConfig.to) {
        client.assert.urlEquals(String(pageConfig.to));
      }

      if (pageConfig.title) {
        client.assert.title(pageConfig.title);
      }
    })

    // Check browser log for errors and warnings
    .getLog('browser', function(logEntriesArray) {
      console.log(`  Browser log length: ${logEntriesArray.length}`);

      if (pageConfig.verbose) {
        console.log(logEntriesArray);
      }

      logEntriesArray.forEach(function(log) {
        let whitelisted = utils.whitelist(pageConfig.logWhitelist, log.message);

        if (whitelisted) {
          if (pageConfig.verbose) {
            console.log(chalk.yellow('Skipping: Substring in message whitelisted…'));
            console.log(log.message);
          }

          return;
        }

        if (log.message.includes('404') && errors.resource.test) {
          errors.resource.count++;
          console.log(chalk.red(`\n[404] ${log.message}`));
        } else if (log.level === 'SEVERE') {
          errors.severe.count++;
          console.log(chalk.red(`\n[${log.level}] ${log.message}`));
        } else if (log.message.includes('eclaration dropped') ||
          log.message.includes('xpected declaration') ||
          log.message.includes('nknown pseudo-class')) {
          if (errors.css.test) {
            errors.css.count++;
            console.log(chalk.yellow(`\n[CSS] ${log.message}`));
          }
        } else if (log.level === 'WARNING') {
          let logMessage;

          // Do-nothing exceptions for firefox warnings
          if (log.message.includes('re-register CID') ||
              log.message.includes('\'binary-component\' directive') ||
              log.message.includes('nsIOService')) {
            return;
          } else if (log.message.includes('eprecated') && errors.deprecation.test) {
            errors.deprecation.count++;

            return console.log(chalk.yellow(`\n[DEPRECATED] ${log.message}`));
          }

          try {
            logMessage = JSON.parse(log.message).message || {};
          } catch (e) {
            logMessage = {};
          }

          if (errors.warning.test && logMessage.level !== 'log') {
            errors.warning.count++;
            console.log(chalk.yellow(`\n[${log.level}] ${log.message}`));
          }
        }
      });

      for (let key in errors) {
        let err = errors[key];

        if (err.test) {
          this.assert.equal(err.count, 0, `Page has no ${key} errors logged`);
        }
      }
    })

    // Look for text content (if supplied in config)
    .perform(function() {
      pageConfig.containsText.forEach(function(searcher) {
        searcher.selector = searcher.selector || 'body';

        if (searcher.text) {
          client.assert.containsText(searcher.selector, searcher.text);
        }
      });
    })

    // Look for elements
    .perform(function() {
      pageConfig.containsElement.forEach(function(searcher) {
        if (!searcher.selector) {
          return;
        }
        searcher.selector = searcher.selector || 'body';
        searcher.count = searcher.count != null ? searcher.count : -1;
        searcher.strategy = searcher.strategy || 'css selector';

        if (searcher.wait) {
          client.waitForElementPresent(searcher.selector, searcher.wait, false, '');
        }

        if (searcher.count !== -1) {
          client.elements(searcher.strategy, searcher.selector, function(json) {
            this.assert.strictEqual(json.state, 'success', 'Expected DOM elements found');
            this.assert.strictEqual(json.value.length, searcher.count, 'Correct number of elements found');
          });
        } else {
          client.element(searcher.strategy, searcher.selector, function(json) {
            this.assert.strictEqual(json.state, 'success', 'Expected DOM elements found');
          });
        }
      });
    })

    // Look for visible elements
    .perform(function() {
      pageConfig.containsVisibleElement.forEach(function(searcher) {
        if (!searcher.selector) {
          return;
        }

        if (searcher.wait) {
          client.waitForElementVisible(searcher.selector, searcher.wait, false, 'Tested [visible]: Element %s was visible after %d milliseconds');
        } else {
          client.assert.visible(searcher.selector);
        }
      });
    })

    // Look for hidden elements
    .perform(function() {
      pageConfig.containsHiddenElement.forEach(function(searcher) {
        if (!searcher.selector) {
          return;
        }

        if (searcher.wait) {
          client.waitForElementNotVisible(searcher.selector, searcher.wait, false, 'Tested [hidden]: Element %s was hidden after %d milliseconds');
        } else {
          client.assert.hidden(searcher.selector);
        }
      });

    })

    // Look for global vars we expect to be defined (globalExposedVars)
    .execute(function(varsToLookFor) {
      var foundGlobeVars = [];

      varsToLookFor.forEach(function(globeVar) {
        if (window[globeVar]) {
          foundGlobeVars.push(globeVar);
        }
      });

      return foundGlobeVars;
    }, [pageConfig.globalExposedVars], function(result) {
      if (pageConfig.globalExposedVars.length) {
        this.assert.deepEqual(JSON.stringify(result.value), JSON.stringify(pageConfig.globalExposedVars), 'All expected global variables could be found');
      }
    })

    // Look for global vars we hope to not find (globalProtectedVars)
    .execute(function(varsToLookFor) {
      var foundGlobeVars = [];

      varsToLookFor.forEach(function(globeVar) {
        if (window[globeVar]) {
          foundGlobeVars.push(globeVar);
        }
      });

      return foundGlobeVars;
    }, [pageConfig.globalProtectedVars], function(result) {
      if (pageConfig.globalProtectedVars.length) {
        this.assert.deepEqual(JSON.stringify(result.value), JSON.stringify([]), 'No protected global variables could be found');
      }
    })
    .end();
  };
});

module.exports = nwTests;

'use strict';

// ~~~~~~~~~~~~~~~~~~~~~~~~ STARTER TEMPLATE ~~~~~~~~~~~~~~~~~~~~
// For running functional tests on multiple pages.
// Copy the file to the test/ directory (one above this directory) and remove the '-example' bit

let domains = {
  local: 'http://localhost:3000',
  development: 'http://ah.fusionarydev.com',
  production: 'http://affin-hwang.com',
};

let requests = [
  {
    // Url or IP we're going to test
    url: 'http://digitalrealty.com/data-center-solutions/',
    // Expect url of 'url' to be redirected to 'to' (if specified):
    to: 'https://www.digitalrealty.com/data-center-solutions/',
    // Response code we expect Default: 200
    response: 200,
    // Look if a response header is present, and (if specified) see if matches pattern
    responseHeaders: [
      {name: 'cache-control', pattern: /no/},
      {name: 'access-control-allow-credentials'},
    ],
    // A string we expect to match the page title - case sensitive
    title: 'Data Center and Colocation Solutions | Digital Realty',
    // If we should expect all resources to load, Default: true
    strictOnResources: true,
    // If we should care about CSS declaration errors, Default: false
    // (Mostly a thing on Firefox)
    strictOnCSS: false,
    // If we should care about depreciation-related warnings, Default: true
    strictOnDeprecated: false,
    // If we should care about misc. warning messages, Default: true
    strictOnWarnings: false,
    // An array of text to search for on the page, and selector. Selector default: body
    // always uses CSS selectors
    containsText: [
      {selector: 'body', text: 'Sustainability'},
      {text: 'Security & Compliance'},
      {selector: '.Block-content', text: 'custom-built'},
    ],
    // An array of DOM elements to search for on the page.
    // selector: required search
    // count: the number of elements we expect to qualify, -1 if how many doesn't matter Default: -1
    // wait: the number of milliseconds to wait for at least one element to be present. Default: 0
    // strategy: ONE of 'class name', 'css selector', 'id', 'name', 'xpath','link text',
    //          'partial link text', 'tag name', 'xpath'. Default: 'css selector'
    // reference: https://github.com/SeleniumHQ/selenium/wiki/JsonWireProtocol#sessionsessionidelement
    containsElement: [
      {selector: '/html/body/div[2]/div[1]/div[6]/div', strategy: 'xpath', count: -1},
      {selector: '.u-md-size1of2', count: 2, wait: 500},
    ],
    // An array of DOM elements to check for visibility and existence
    // selector: required search
    // wait: the number of milliseconds to wait for the element to be visible. Default: 0
    containsVisibleElement: [
      {
        selector: '.ContentElements>:first-child, .ContentElements h5+p',
      },
      {
        selector: '.dlr-Menu',
        wait: 400,
      }
    ],
    // An array of CSS selectors to check for "invisibility" and existence
    // selector: required search
    // wait: the number of milliseconds to wait for the element to be present but hidden. Default: 0
    containsHiddenElement: [
      {
        selector: '.js-backToTop'
      }
    ],
    // Exposed JS variables we expect to find, Default: []
    // (Check for 'ga' or 'GoogleAnalyticsObject' for analytics)
    globalExposedVars: ['$', 'ga'],
    // Exposed JS variables we don't want to find (like passwords), Default: []
    globalProtectedVars: ['pass', 'UID', 'GUID'],
  },
  {
    url: 'http://www.exmouth-view.co.uk/',
    to: 'http://www.exmouth-view.co.uk/',
    response: 200,
    responseHeaders: [],
    title: 'Exmouth View Hotel Babbacombe, Family Friendly Babbacombe Hotel, Favourite Torquay Hotels. A Family Owned and Run, Family Friendly Hotel, Self Drive Hotel, Children Welcome in Babbacombe Torquay - Fawlty Towers Re-Opened - Affordable Accomodation @ Exmout',
    strictOnResources: false,
    strictOnCSS: false,
    strictOnInternal: true,
    strictOnWarnings: true,
    containsText: [
      {text: 'The Man They Couldn\'t Hang !'},
    ],
    containsElement: [],
    containsVisibleElement: [{selector: '.auto-style23'}],
    containsHiddenElement: [],
    globalExposedVars: [],
    globalProtectedVars: [],
  },
  {
    name: 'ah',
    path: 'search/?q=</title><script>alert(\'XSS Wins\');</script><title>',
  },
];

module.exports = {
  requests: requests,
  domains: domains,
};

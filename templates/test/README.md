# Deployment tests

## Set up / prerequisites

* Java. You should have this installed already. If not, you can install it via homebrew.
* Selenium server: get the latest non-beta version from http://selenium-release.storage.googleapis.com/index.html
* Chrome webdriver: get the latest build for Mac from http://chromedriver.storage.googleapis.com/index.html

**IMPORTANT**: If you get a session error when running the Chrome tests, you might need to update the Chrome webdriver. Just go to the URL above and find the latest. The error output will start like this:

```
Error retrieving a new session from the selenium server
Connection refused! Is selenium server started?
```

Download and unzip the Selenium server and Chrome webdriver to a folder on your machine (NOT in the repo).
In your `.env` file, add a line that points `TEST_BIN` to the folder where you put those files. For example:

```bash
TEST_BIN=/Users/YOURNAME/bin
```

While still in `.env`, add another line for the test environment. For example:

```bash
TEST_ENV=local
```

**Note:** If the site is already in production, you should probably do `TEST_ENV=production` so the tests run against the live site.

## IMPORTANT: Recent Firefox Workaround

The selenium server doesn't work with recent versions of Firefox. Until the issue is resolved, you need to jump through a couple extra hoops if you want to run tests in Firefox:

* Download Firefox 45.3ESR from https://ftp.mozilla.org/pub/firefox/releases/45.3.0esr/mac/en-US/
* Open the .DMG and drag the Firefox icon to the `TEST_BIN` directory you identified above.

## Running tests

To run tests in both Firefox and Chrome, do the following from the command line:

```bash
npm test
```

To run tests in Chrome only:

```bash
npm run test-chrome
```

To run tests in Firefox only:

```bash
npm run test-firefox
```

## Configuring tests

You might want to start with `test/pages-example.js` to see all the available options.

### Domains

Add domains for each environment. If you include a `path` property for a page, the tests will combine the domain for the environment set by `TEST_ENV` in the `.env` file and the path.

For example, let's say you've set `TEST_ENV=production` in `.env` and your `domains` object looks like this:

```js
let domains = {
  local: 'http://localhost:3000',
  development: 'http://paddling.fusionarydev.com',
  production: 'https://paddling.com',
};
```

If you set the `path` property to `foo/bar/`, the tests will open `https://paddling.com/foo/bar/`.

### Requests
Add pages that you would like to test in `test/pages.js`. The `requests` variable should be an array of objects, with each page represented by an object.

You must have either a `path` or a `url` property for each page:

```js
let testPages = [
  {
    url: 'https://google.com/'
  },
  {
    path: 'path/to/my/page/'
  }
];
```
